---
matrix_synapse_version: v1.81.0
matrix_synapse_image: registry.gitlab.com/guardianproject-ops/docker-synapse:{{ matrix_synapse_version }}
matrix_local_bin_path: "/usr/local/bin"
matrix_server_fqn_matrix: matrix.{{ matrix_domain }}

matrix_synapse_enabled: true
matrix_synapse_base_path: "{{ matrix_data_dir }}/synapse"
matrix_synapse_config_dir_path: "{{ matrix_synapse_base_path }}/config"
matrix_synapse_storage_path: "{{ matrix_synapse_base_path }}/storage"
matrix_synapse_media_store_path: "{{ matrix_synapse_storage_path }}/media"
matrix_synapse_ext_path: "{{ matrix_synapse_base_path }}/ext"
matrix_synapse_id_servers_public: ["matrix.org", "vector.im"]
matrix_synapse_default_identity_server: https://matrix.org
matrix_synapse_enable_registration_without_verification: false

# matrix_synapse_admin_contact_email:

# The list of identity servers to use for Synapse.
# We assume this role runs standalone without a local Identity server, so we point Synapse to public ones.
# This most likely gets overwritten later, so that a local Identity server is used.
matrix_synapse_trusted_third_party_id_servers: "{{ matrix_synapse_id_servers_public }}"

matrix_synapse_template_synapse_homeserver: "{{ role_path }}/templates/synapse/homeserver.yaml.j2"
matrix_synapse_template_synapse_log: "{{ role_path }}/templates/synapse/synapse.log.config.j2"

# matrix_synapse_signing_key: ""
matrix_synapse_macaroon_secret_key: ""
matrix_synapse_registration_shared_secret: "{{ matrix_synapse_macaroon_secret_key }}"
matrix_synapse_allow_guest_access: false
matrix_synapse_form_secret: "{{ matrix_synapse_macaroon_secret_key }}"

matrix_synapse_max_upload_size_mb: 1024

# The tmpfs at /tmp needs to be large enough to handle multiple concurrent file uploads.
matrix_synapse_tmp_directory_size_mb: "{{ (matrix_synapse_max_upload_size_mb | int) * 4 }}"

# Log levels
# Possible options are defined here https://docs.python.org/3/library/logging.html#logging-levels
# warning: setting log level to DEBUG will make synapse log sensitive information such
# as access tokens.
#
# Increasing verbosity may lead to an excessive amount of log messages being generated,
# some of which may get dropped by systemd-journald on certain distributions (like CentOS 7).
# You can work around it by adding `RateLimitInterval=0` and `RateLimitBurst=0` under `[Storage]` in
# `/etc/systemd/journald.conf` and restarting the logging service (`systemctl restart systemd-journald`).
matrix_synapse_log_level: "WARNING"
matrix_synapse_storage_sql_log_level: "WARNING"
matrix_synapse_root_log_level: "WARNING"

# Log retention
#
# @var matrix_synapse_logrotate_days: 1 # Logs are rotated after this many days
matrix_synapse_logrotate_days: 1
# @var matrix_synapse_logrotate_maxsize: 100000000 # Log files are rotated when they grow bigger than size bytes even before the daily interval
matrix_synapse_logrotate_maxsize: 100000000

# Rate limits
matrix_synapse_rc_message:
  per_second: 0.2
  burst_count: 10

matrix_synapse_rc_registration:
  per_second: 0.17
  burst_count: 3

matrix_synapse_rc_login:
  address:
    per_second: 0.17
    burst_count: 3
  account:
    per_second: 0.17
    burst_count: 3
  failed_attempts:
    per_second: 0.17
    burst_count: 3

matrix_synapse_rc_federation:
  window_size: 1000
  sleep_limit: 10
  sleep_delay: 500
  reject_limit: 50
  concurrent: 3

matrix_synapse_federation_rr_transactions_per_room_per_second: 50

# Controls whether the TLS federation listener is enabled (tcp/8448).
# Only makes sense if federation is enabled (`matrix_synapse_federation_enabled`).
# Note that federation may potentially be enabled as non-TLS on tcp/8048 as well.
# If you're serving Synapse behind an HTTPS-capable reverse-proxy,
# you can disable the TLS listener (`matrix_synapse_tls_federation_listener_enabled: false`).
matrix_synapse_tls_federation_listener_enabled: false
matrix_synapse_tls_certificate_path: "{{ matrix_synapse_config_dir_path }}/{{ matrix_server_fqn_matrix }}.tls.crt"

# Resource names used by the unsecure HTTP listener. Here only the Client API
# is defined, see the homeserver config for a full list of valid resource
# names.
matrix_synapse_http_listener_resource_names: ["client"]

# Enable this to allow Synapse to report utilization statistics about your server to matrix.org
# (things like number of users, number of messages sent, uptime, load, etc.)
matrix_synapse_report_stats: false

# Controls whether the Matrix server will track presence status (online, offline, unavailable) for users.
# If users participate in large rooms with many other servers,
# disabling this will decrease server load significantly.
matrix_synapse_use_presence: true

# Controls whether accessing the server's public rooms directory can be done without authentication.
# For private servers, you most likely wish to require authentication,
# unless you know what list of rooms you're publishing to the world and explicitly want to do it.
matrix_synapse_allow_public_rooms_without_auth: false

# Controls whether remote servers can fetch this server's public rooms directory via federation.
# For private servers, you most likely wish to forbid it.
matrix_synapse_allow_public_rooms_over_federation: false

# Controls whether people with access to the homeserver can register by themselves.
matrix_synapse_registration_enabled: false

# reCAPTCHA API for validating registration attempts
matrix_synapse_registration_enabled_captcha: false
matrix_synapse_recaptcha_public_key: ""
matrix_synapse_recaptcha_private_key: ""

# Allows non-server-admin users to create groups on this server
matrix_synapse_enable_group_creation: true

# A list of 3PID types which users must supply when registering (possible values: email, msisdn).
matrix_synapse_registrations_require_3pid: []

# A list of patterns 3pids must match in order to permit registration, e.g.:
#  - medium: email
#    pattern: '.*@example\.com'
#  - medium: msisdn
#    pattern: '\+44'
matrix_synapse_allowed_local_3pids: []

# The server to use for email threepid validation. When empty, Synapse does it by itself.
# Otherwise, this should be pointed to an identity server.
matrix_synapse_account_threepid_delegates_email: ""

# The server to use for phone number threepid validation. When empty, validation cannot happen, as Synapse doesn't support it.
# To make it work, this should be pointed to an identity server.
matrix_synapse_account_threepid_delegates_msisdn: ""

# Users who register on this homeserver will automatically be joined to these rooms.
# Rooms are to be specified using addresses (e.g. `#address:example.com`)
matrix_synapse_auto_join_rooms: []

# Controls whether auto-join rooms (`matrix_synapse_auto_join_rooms`) are to be created
# automatically if they don't already exist.
matrix_synapse_autocreate_auto_join_rooms: true

# Controls password-peppering for Synapse. Not to be changed after initial setup.
matrix_synapse_password_config_pepper: ""

# Controls if Synapse allows people to authenticate against its local database.
# It may be useful to disable this if you've configured additional password providers
# and only wish authentication to happen through them.
matrix_synapse_password_config_localdb_enabled: true

# Controls the number of events that Synapse caches in memory.
matrix_synapse_event_cache_size: "100K"

# Controls cache sizes for Synapse.
# Raise this to increase cache sizes or lower it to potentially lower memory use.
# To learn more, see:
# - https://github.com/matrix-org/synapse#help-synapse-eats-all-my-ram
# - https://github.com/matrix-org/synapse/issues/3939
matrix_synapse_caches_global_factor: 0.5

# Controls whether Synapse will federate at all.
# Disable this to completely isolate your server from the rest of the Matrix network.
# Also see: `matrix_synapse_tls_federation_listener_enabled` if you wish to keep federation enabled,
# but want to stop the TLS listener (port 8448).
matrix_synapse_federation_enabled: true

matrix_synapse_federation_port: 8448

# A list of domain names that are allowed to federate with the given Synapse server.
# An empty list value (`[]`) will also effectively stop federation, but if that's the desired
# result, it's better to accomplish it by changing `matrix_synapse_federation_enabled`.
matrix_synapse_federation_domain_whitelist: ~

# A list of additional loggers to register in synapse.log.config.
# This list gets populated dynamically based on Synapse extensions that have been enabled.
# Contains definition objects like this: `{"name": "..", "level": "DEBUG"}
matrix_synapse_additional_loggers: []

# A list of appservice config files (in-container filesystem paths).
# This list gets populated dynamically based on Synapse extensions that have been enabled.
# You may wish to use this together with `matrix_synapse_container_additional_volumes` or `matrix_synapse_container_extra_arguments`.
matrix_synapse_app_service_config_files: []

# This is set dynamically during execution depending on whether
# any password providers have been enabled or not.
matrix_synapse_password_providers_enabled: false

# Whether clients can request to include message content in push notifications
# sent through third party servers. Setting this to false requires mobile clients
# to load message content directly from the homeserver.
matrix_synapse_push_include_content: false

# If url previews should be generated. This will cause a request from Synapse to
# URLs shared by users.
matrix_synapse_url_preview_enabled: true

# Enable exposure of metrics to Prometheus
# See https://github.com/matrix-org/synapse/blob/master/docs/metrics-howto.md
matrix_synapse_metrics_enabled: false
matrix_synapse_metrics_bind_addresses:
  - 0.0.0.0
matrix_synapse_metrics_port: 9202

# Enable the Synapse manhole
# See https://github.com/matrix-org/synapse/blob/master/docs/manhole.md
matrix_synapse_manhole_enabled: false

# Send ERROR logs to sentry.io for easier tracking
# To set this up: go to sentry.io, create a python project, and set
# matrix_synapse_sentry_dsn to the URL it gives you.
# See https://github.com/matrix-org/synapse/issues/4632 for important privacy concerns
matrix_synapse_sentry_dsn: ""

# Postgres database information
matrix_synapse_db_hostname: ""
matrix_synapse_db_username: ""
matrix_synapse_db_password: ""
matrix_synapse_db_name: ""
matrix_synapse_db_port: 5432
matrix_synapse_db_cp_min: 5
matrix_synapse_db_cp_max: 10

matrix_synapse_turn_uris: []
matrix_synapse_turn_shared_secret: ""
matrix_synapse_turn_allow_guests: false

matrix_synapse_email_enabled: false
matrix_synapse_email_smtp_hostname: ""
matrix_synapse_email_smtp_port: 587
# matrix_synapse_email_smtp_username:
# matrix_synapse_email_smtp_password:
matrix_synapse_email_smtp_require_transport_security: false
matrix_synapse_email_notif_from: "Matrix <matrix@{{ matrix_domain }}>"
matrix_synapse_email_client_base_url: "https://{{ matrix_server_web_client_domain }}"
matrix_synapse_email_app_name: Matrix

matrix_s3_media_store_enabled: false
matrix_s3_media_store_custom_endpoint_enabled: false
matrix_s3_storage_provider_git_ref: master
matrix_s3_storage_provider_pip_url: git+https://github.com/matrix-org/synapse-s3-storage-provider.git@{{ matrix_s3_storage_provider_git_ref }}
matrix_s3_media_store_bucket_name: "your-bucket-name"
matrix_s3_media_store_region: "eu-central-1"
matrix_s3_media_store_class: STANDARD
matrix_s3_media_store_threadpool_size: 40

# Controlrs whether the user directory seearch feature is enabled.
matrix_synapse_user_directory_enabled: false

# Controls whether the self-check feature should validate SSL certificates.
matrix_synapse_self_check_validate_certificates: true

# Controls whether locally-created rooms should be end-to-end encrypted by
# default.
# Possible options are "all", "invite", and "off" (refer to homeserver.yaml.tmpl for details)
matrix_synapse_encryption_enabled_by_default_for_room_type: all

# Controls whether searching the public room list is enabled.
matrix_synapse_enable_room_list_search: true

# Controls who's allowed to create aliases on this server.
matrix_synapse_alias_creation_rules:
  - user_id: "*"
    alias: "*"
    room_id: "*"
    action: allow

# Controls who can publish and which rooms can be published in the public room list.
matrix_synapse_room_list_publication_rules:
  - user_id: "*"
    alias: "*"
    room_id: "*"
    action: allow

matrix_synapse_default_room_version: "5"

# Controls the Synapse `spam_checker` setting.
#
# If a spam-checker extension is enabled, this variable's value is set automatically by the playbook during runtime.
# If not, you can also control its value manually.
matrix_synapse_spam_checker: []

matrix_synapse_trusted_key_servers:
  - server_name: "matrix.org"

matrix_synapse_redaction_retention_period: 7d

matrix_synapse_user_ips_max_age: 28d

matrix_synapse_configuration_yaml: "{{ lookup('template', 'templates/synapse/homeserver.yaml.j2') }}"

matrix_synapse_configuration_extension_yaml: |
  # Your custom YAML configuration for Synapse goes here.
  # This configuration extends the default starting configuration (`matrix_synapse_configuration_yaml`).
  #
  # You can override individual variables from the default configuration, or introduce new ones.
  #
  # If you need something more special, you can take full control by
  # completely redefining `matrix_synapse_configuration_yaml`.
  #
  # Example configuration extension follows:
  #
  # server_notices:
  #   system_mxid_localpart: notices
  #   system_mxid_display_name: "Server Notices"
  #   system_mxid_avatar_url: "mxc://server.com/oumMVlgDnLYFaPVkExemNVVZ"
  #   room_name: "Server Notices"

matrix_synapse_configuration_extension: "{{ matrix_synapse_configuration_extension_yaml|from_yaml if matrix_synapse_configuration_extension_yaml|from_yaml is mapping else {} }}"

# Holds the final Synapse configuration (a combination of the default and its extension).
# You most likely don't need to touch this variable. Instead, see `matrix_synapse_configuration_yaml`.
# matrix_synapse_configuration: "{{ matrix_synapse_configuration_yaml|from_yaml|combine(matrix_synapse_configuration_extension, recursive=True) }}"
matrix_synapse_configuration: "{{ matrix_synapse_configuration_yaml|from_yaml}}"
