## Role Variables

* `matrix_synapse_logrotate_days`: `1` - Logs are rotated after this many days



* `matrix_synapse_logrotate_maxsize`: `100000000` - Log files are rotated when they grow bigger than size bytes even before the daily interval


