SHELL = /bin/bash
export README_DEPS ?=  docs/ansible.md
-include $(shell curl -sSL -o .build-harness "https://gitlab.com/snippets/1957473/raw"; echo .build-harness)
export CI_PROJECT_PATH_SLUG ?= devlocal_$(shell basename $(shell pwd))
export CI_COMMIT_REF_SLUG   ?= $(shell git rev-parse --abbrev-ref HEAD)
export CI_COMMIT_SHORT_SHA  ?= $(shell git rev-parse --short HEAD)
export ENV_FILE ?= ../../../.env

export MOLECULE_AMI_DISTRO ?= "debian-10-amd64-*"
export MOLECULE_AMI_OWNER ?= "136693071363"

converge:
	source $(ENV_FILE) && molecule converge

verify:
	source $(ENV_FILE) && molecule verify

destroy:
	source $(ENV_FILE) && molecule destroy

lint:
	source $(ENV_FILE) && molecule lint

test:
	source $(ENV_FILE) && molecule test --all
